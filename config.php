<?php

define('CONTENT_PATH', __DIR__ . '/public/_content');

define('ALLOWED_FILE_EXTENSIONS', ['md', 'png', 'jpg', 'jpeg']);
