<?php

require_once __DIR__ . '/../../vendor/autoload.php';

require_once __DIR__ . '/../../config.php';
require_once __DIR__ . '/../lib/Controller.php';
require_once __DIR__ . '/../lib/Tools.php';

class FileController extends Controller {
    static function put() {
        if (!isset($_GET['path']) || !isset($_GET['name'])) {
            return new Response(400, 'text/plain', 'Missing params');
        }
        
        $path = realpath(CONTENT_PATH . $_GET['path']);

        if ($path === false || strpos($path, CONTENT_PATH) !== 0) {
            return new Response(400, 'text/plain', 'Invalid path');
        }

        if (str_contains($_GET['name'], '/') || str_starts_with($_GET['name'], '_')) {
            return new Response(400, 'text/plain', 'Invalid name');
        }

        $file_read = fopen('php://input', 'r');
        $file_write = fopen($path . '/' . $_GET['name'], 'w');

        $success = stream_copy_to_stream($file_read, $file_write);

        fclose($file_read);
        fclose($file_write);

        if (!$success) {
            return new Response(500, 'text/plain', 'Failed to save file');
        }

        $tree = Tools::getDirectoryTree(CONTENT_PATH);

        return new Response(200, 'application/json', json_encode($tree));
    }
}
