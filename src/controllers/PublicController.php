<?php

require __DIR__ . '/../../vendor/autoload.php';

require_once __DIR__ . '/../../config.php';
require_once __DIR__ . '/../lib/Controller.php';

class PublicController extends Controller {
    static function get() {
        $templates = new League\Plates\Engine(__DIR__ . '/../templates', 'phtml');

        $path = false;

        foreach(['.md', '/index.md', 'index.md'] as $suffix) {
            $path = realpath(CONTENT_PATH . $_SERVER['REQUEST_URI'] . $suffix);

            if ($path !== false) {
                break;
            }
        }

        if ($path === false || strpos($path, CONTENT_PATH) !== 0) {
            return new Response(404, 'text/html', $templates->render('404'));
        }

        $content = (new Parsedown())->text(file_get_contents($path));

        return new Response(
            200,
            'text/html',
            $templates->render('page', ['content' => $content])
        );
    }
}

