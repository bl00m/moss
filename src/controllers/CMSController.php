<?php

require_once __DIR__ . '/../../vendor/autoload.php';

require_once __DIR__ . '/../../config.php';
require_once __DIR__ . '/../lib/Controller.php';
require_once __DIR__ . '/../lib/Tools.php';

class CMSController extends Controller {
    static function get() {
        $templates = new League\Plates\Engine(__DIR__ . '/../templates', 'phtml');

        $js_directory_tree = [
            'js_directory_tree' => Tools::getDirectoryTree(CONTENT_PATH)
        ];

        return new Response(
            200,
            'text/html',
            $templates->render('editor', ['js_directory_tree' => $js_directory_tree])
        );
    }

    static function post() {
        if (!isset($_POST['type']) || !isset($_POST['path']) || !isset($_POST['name'])) {
            return new Response(400, 'text/plain', 'Missing params');
        }

        $path = realpath(CONTENT_PATH . $_POST['path']);

        if ($path === false || strpos($path, CONTENT_PATH) !== 0) {
            return new Response(400, 'text/plain', 'Invalid path');
        }

        if (str_contains($_POST['name'], '/') || str_starts_with($_POST['name'], '_')) {
            return new Response(400, 'text/plain', 'Invalid name');
        }

        $new_path = $path . '/' . $_POST['name'];

        if (realpath($new_path) !== false) {
            return new Response(400, 'text/plain', 'File or directory already exists');
        }

        if ($_POST['type'] == 'file') {
            $pathinfo = pathinfo($new_path);

            if (!array_key_exists('extension', $pathinfo) || $pathinfo['extension'] !== 'md') {
                return new Response(400, 'text/plain', 'Only .md files are accepted');
            }

            file_put_contents($new_path, '');

        } else if ($_POST['type'] == 'directory') {
            mkdir($new_path, 0755);

        } else {
            return new Response(400, 'text/plain', 'Unexpected type');
        }

        $tree = Tools::getDirectoryTree(CONTENT_PATH);

        return new Response(200, 'application/json', json_encode($tree));
    }

    static function put() {
        $input_markdown = file_get_contents('php://input');
        $content = (new Parsedown())->text($input_markdown);

        if (isset($_GET['save']) && $_GET['save']) {
            $path = realpath(CONTENT_PATH . $_GET['path']);

            if ($path === false) {
                return new Response(400, 'text/plain', '404');
            }

            if (strpos($path, CONTENT_PATH) !== 0) {
                return new Response(400, 'text/plain', 'Invalid path');
            }

            if ($path) {
                file_put_contents($path, $input_markdown);
            }
        }

        $templates = new League\Plates\Engine(__DIR__ . '/../templates', 'phtml');

        return new Response(
            200,
            'text/html',
            $templates->render('page', ['content' => $content])
        );
    }

    static function delete() {
        if (!isset($_GET['path'])) {
            return new Response(400, 'text/plain', 'Missing path');
        }

        $path = realpath(CONTENT_PATH . $_GET['path']);

        if ($path === false) {
            return new Response(400, 'text/plain', 'Not found');
        }

        if (strpos($path, CONTENT_PATH) !== 0) {
            return new Response(400, 'text/plain', 'Invalid path');
        }

        if ($path === CONTENT_PATH) {
            return new Response(400, 'text/plain', 'Cannot delete ' . $_GET['path']);
        }

        $pathinfo = pathinfo($path);

        if (is_file($path) && (!array_key_exists('extension', $pathinfo) || $pathinfo['extension'] !== 'md')) {
            return new Response(400, 'text/plain', 'Only .md files can be deleted');
        }

        $success = Tools::recurseRm($path);

        if (!$success) {
            return new Response(500, 'text/plain', 'Failed to delete ' . $_GET['path']);
        }

        $tree = Tools::getDirectoryTree(CONTENT_PATH);

        return new Response(200, 'application/json', json_encode($tree));
    }
}

