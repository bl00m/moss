<?php

class Response {
    public function __construct($http_code, $content_type="text/plain", $content="") {
        $this->http_code = $http_code;
        $this->content_type = $content_type;
        $this->content = $content;
    }

    public function send() {
        http_response_code($this->http_code);
        header("Content-Type: $this->content_type");
        echo $this->content;
        exit($this->http_code >= 500 ? 1 : 0);
    }
}

abstract class Controller {
    static function handleRequest() {
        $called_class = get_called_class();
        $method_name = strtolower($_SERVER['REQUEST_METHOD']);

        try {
            if (method_exists($called_class, $method_name)) {
                $resp = $called_class::{$method_name}();
                $resp->send();

            } else {
                $resp = new Response(
                    400,
                    'text/plain',
                    "Unsupported HTTP method: " . $_SERVER['REQUEST_METHOD']
                );
                $resp->send();
            }

        } catch (Throwable $e) {
            self::logError($e);

            $resp = new Response(500, 'text/plain', "Unexpected server error");
            $resp->send();
        }
    }

    private static function logError($error) {
        $log_msg = implode('|', [
            date(DATE_ATOM),
            $error->getFile() . ':' . $error->getLine(),
            $error->getMessage(),
        ]) . "\n";

        error_log($log_msg, 3, __DIR__ . '/../../var/logs/error.log');
    }
}

