<?php
class Tools {
    static function getDirectoryTree($root) {
        if (is_string($root)) {
            $root = new SplFileInfo($root);
        }

        $tree = [
            'name' => $root->getFilename() . '/',
            'path' => substr($root->getRealPath(), strlen(CONTENT_PATH)) . '/',
            'files' => [],
            'children' => []
        ];

        $dir = new DirectoryIterator($root->getRealPath());

        foreach ($dir as $fileinfo) {
            if ($fileinfo->isDir() && !$fileinfo->isDot()) {
                $child = self::getDirectoryTree($fileinfo);
                $tree['children'][] = $child;

            } else if (in_array($fileinfo->getExtension(), ALLOWED_FILE_EXTENSIONS)) {
                $tree['files'][] = [
                    'name' => $fileinfo->getFilename(),
                    'path' => substr($fileinfo->getRealPath(), strlen(CONTENT_PATH))
                ];
            }
        }

        return $tree;
    }

    static function recurseRm($path) {
        if (is_file($path)) {
            return unlink($path);
        }

        $files = array_diff(scandir($path), array('.','..'));

        foreach ($files as $file) {
            self::recurseRm("$path/$file");
        }

        return rmdir($path);
    }
}

