class App {
    constructor(el) {
        this.el = el
        this.refs = {}

        this.switchView = this.switchView.bind(this)

        this.el.getElementById('view-switch')
            .addEventListener('click', this.switchView)
    }

    start() {
        this.refs = {
            browserTree: new BrowserTree(this, this.el.getElementById('tree')),
            preview: new Preview(this, this.el.getElementById('preview')),
            editForm: new EditForm(this, this.el.getElementById('editor')),
            createForm: new CreateForm(this, this.el.getElementById('create')),
            uploadForm: new UploadForm(this, this.el.getElementById('upload'))
        }

        this.refs.browserTree.loadTree(DIRECTORY_TREE)
        this.loadPage('/index.md')
    }

    async loadPage(path) {
        const content = await fetch(path.replace('.md', ''))
            .then(resp => resp.text())

        this.refs.preview.setContent(content)

        const resp = await fetch(`/_content${path}`)

        this.refs.editForm.setValues({
            path,
            content: resp.status !== 404
                ? await resp.text()
                : ''
        })

        this.refs.browserTree.selectLink(path)

        this.refs.createForm.hide()
        this.refs.uploadForm.hide()
    }

    switchView(event) {
        if (event.target.textContent === 'Edit') {
            this.refs.editForm.show()
            this.el.querySelector('#browser').classList.add('hidden')
            event.target.textContent = 'Browse'

        } else {
            this.refs.editForm.hide()
            this.el.querySelector('#browser').classList.remove('hidden')
            event.target.textContent = 'Edit'
        }

        this.refs.createForm.hide()
        this.refs.uploadForm.hide()
    }
}

class BrowserTree {
    static fileActions = [
        { label: "❌", title: "Delete this file", action: "delete" }
    ]

    static directoryActions = [
        { label: "➕", title: "Add a new page/directory", action: "add" },
        { label: "⬆️", title: "Upload a file", action: "upload" },
        { label: "❌", title: "Delete this directory", action: "delete" }
    ]

    constructor(app, el) {
        this.app = app
        this.el = el

        this.onDirectoryClick = this.onDirectoryClick.bind(this)
        this.onFileClick = this.onFileClick.bind(this)
        this.onActionClick = this.onActionClick.bind(this)
    }

    loadTree(data) {
        this.el.textContent = ''
        this.el.appendChild(this.buildDirectory(data))
    }

    selectLink(path) {
        this.el.querySelector('.item--file-selected')
            ?.classList.remove('item--file-selected')

        this.el.querySelector(`a[data-path="${path}"]`)
            ?.parentElement.classList.add('item--file-selected')
    }

    onDirectoryClick(event) {
        event.preventDefault()
        event.target.parentElement.parentElement.classList.toggle('item--directory-folded')
    }

    onFileClick(event) {
        event.preventDefault()

        const path = event.target.dataset.path

        if (path.endsWith('.md')) {
            this.app.loadPage(path)

        } else {
            this.app.refs.preview.load(`/_content${path}`)
        }
    }

    async onActionClick(event) {
        const action = event.target.dataset.action
        const path = event.target.parentElement.previousSibling.dataset.path

        switch (action) {
            case 'add':
                this.app.refs.createForm.setValues({ path })
                this.app.refs.uploadForm.hide()
                this.app.refs.createForm.show()
                break;

            case 'upload':
                this.app.refs.uploadForm.setValues({ path })
                this.app.refs.createForm.hide()
                this.app.refs.uploadForm.show()
                break;

            case 'delete':
                if (confirm(`Delete ${path} ?`)) {
                    const resp = await req('DELETE', '/_cms/', { searchParams: { path } })

                    if (resp.status !== 200) {
                        alert(await resp.text())
                        return
                    }

                    this.loadTree(await resp.json())
                }
                break;
        }
    }

    buildDirectory(data) {
        const div = document.createElement('div')
        div.className = "item item--directory"

        const innerDiv = document.createElement('div')
        div.appendChild(innerDiv)

        const a = document.createElement('a')
        a.href = '#'
        a.textContent = data.name
        a.dataset.path = data.path
        a.addEventListener('click', this.onDirectoryClick)
        innerDiv.appendChild(a)

        const actions = [...BrowserTree.directoryActions]

        if (data.path === '/') {
            a.textContent = '/'
            actions.pop()
        }

        innerDiv.appendChild(this.buildItemActions(actions))

        const ul = document.createElement('ul')
        div.appendChild(ul)

        for (const dir of data.children) {
            const li = document.createElement('li')
            li.appendChild(this.buildDirectory(dir))
            ul.appendChild(li)
        }

        for (const file of data.files) {
            const li = document.createElement('li')
            li.appendChild(this.buildFile(file))
            ul.appendChild(li)
        }

        return div
    }

    buildFile(data) {
        const div = document.createElement('div')
        div.className = "item item--file"

        const a = document.createElement('a')
        a.href = '#'
        a.dataset.path = data.path
        a.textContent = data.name
        a.addEventListener('click', this.onFileClick)
        div.appendChild(a)

        div.appendChild(this.buildItemActions(BrowserTree.fileActions))

        return div
    }

    buildItemActions(actions) {
        const div = document.createElement('div')
        div.className = "item__actions"

        for (const action of actions) {
            const button = document.createElement('button')
            button.textContent = action.label
            button.title = action.title
            button.dataset.action = action.action
            button.addEventListener('click', this.onActionClick)
            div.appendChild(button)
        }

        return div
    }
}

class Preview {
    constructor(app, el) {
        this.app = app
        this.el = el
        this.iframe = this.el.querySelector('iframe')
    }

    async load(url) {
        this.iframe.removeAttribute('srcdoc')
        this.iframe.src = url
    }

    async setContent(content) {
        this.iframe.removeAttribute('src')
        this.iframe.srcdoc = content
    }
}

const toggleVisibleMixin = {
    show() {
        this.el.classList.remove('hidden')
    },

    hide() {
        this.el.classList.add('hidden')
    },

    toggleVisible() {
        if (this.el.classList.contains('hidden')) {
            this.show()

        } else {
            this.hide()
        }
    }
}

class Form {
    constructor(app, el) {
        this.app = app
        this.el = el
        this.el.addEventListener('submit', event => {
            event.preventDefault()

            this.onSubmit(
                new FormData(event.target),
                event.submitter.dataset.action
            )
        })
    }

    setValues(values) {
        for (const name in values) {
            this.el.elements[name].value = values[name]
        }
    } 

    onSubmit(formData, action) {
        alert('Not implemented')
    }
}

class CreateForm extends Form {
    async onSubmit(formData, action) {
        if (action === 'cancel') {
            this.hide()
            return
        }

        const resp = await req('POST', '/_cms/', {
            body: formData
        })

        if (resp.status !== 200) {
            alert(await resp.text())
            return
        }

        this.app.refs.browserTree.loadTree(await resp.json())

        this.hide()
    }
}

Object.assign(CreateForm.prototype, toggleVisibleMixin)

class EditForm extends Form {
    async onSubmit(formData, action) {
        let searchParams = null

        if (action === 'save') {
            searchParams = {
                save: 1,
                path: formData.get('path')
            }
        }

        const content = await req('PUT', '/_cms/', {
                searchParams,
                body: formData.get('content')
            })
            .then(resp => resp.text())

        this.app.preview.setContent(content)
    }
}

Object.assign(EditForm.prototype, toggleVisibleMixin)

class UploadForm extends Form {
    async onSubmit(formData, action) {

        if (action === 'cancel') {
            this.hide()
            return
        }

        const resp = req('PUT', '/_cms/', {
            searchParams: {
                path: formData.get('path'),
                name: formData.get('name')
            },
            body: formData.get('file')
        })

        if (resp.status !== 200) {
            alert(await resp.text())
            return
        }

        this.app.refs.browserTree.loadTree(await resp.json())

        this.hide()
    }
}

Object.assign(UploadForm.prototype, toggleVisibleMixin)

async function req(method, url, options) {
    const fetchOptions = options
        ? { ...options, method }
        : { method }

    if (fetchOptions.searchParams) {
        url += '?' + new URLSearchParams(options.searchParams)
        delete fetchOptions.searchParams
    }

    return fetch(url, fetchOptions)
}

(new App(document.getElementById('app'))).start()
